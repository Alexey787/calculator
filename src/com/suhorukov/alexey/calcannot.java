package com.suhorukov.alexey;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Created with IntelliJ IDEA.
 * User: pc
 * Date: 14.09.13
 * Time: 15:20
 * To change this template use File | Settings | File Templates.
 */
public @interface calcannot {
    @Retention(RetentionPolicy.RUNTIME)
    @Target(ElementType.FIELD)
    @interface Anno {
        int length();
    }

}
